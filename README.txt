RNC weapons updated to MM8BDM V6B by Trillster!
Load with the RNC map pack for cool replacements and effects!

Credits to the original expansion team and those who have continued this pack.

=== Modder Notes ===
Translation IDs 3320-3336 used.
ClassBase Slot 31 used.

=== v4b -> v4c ===
-Added compatibility with CBM
-Optimized RNC map replacement script.
-Circuit Board pickup now removes extra flags

=== v4a -> v4b ===
-Renamed some files to make them more specific to RNC
-Large HP pickup has adjusted respawn cooldown properties
-RNC Item-1 matches V6B Item-1 now
--Removed from assist item and training pool to reduce dupes.
