actor RockBusterUpgrade : BaseMM8BDMBusterUpgrade
{
	//$Category RNC-Weapons
	//$Title Rock Buster Upgrade
	//$Sprite RNCWI0
	Inventory.RespawnTics 350
	inventory.amount 1
	inventory.maxamount 1
	inventory.pickupmessage "$PU_ROCKBUSTER"
	Tag "$TAG_ROCKBUSTERUPGRADE"
	inventory.icon "RNCWP9"
	Inventory.PickupSound "item/1up"
	scale 2.0
	+COUNTITEM
	states
	{
		SpawnLoop:
			RNCW I 1
			loop
		Use:
			RNCW I 0 A_PlaySoundEx("item/refill","Voice")
			RNCW I 0 A_GiveInventory("BusterGiven",1)
			RNCW I 0 A_GiveInventory("RockBuster",1)
			RNCW I 0 A_SelectWeapon("RockBuster")
			stop
	}
}

actor RockBusterUpgrade_Respawn : 8BDMItemRespawn
{
	translation "192:192=102:102", "198:198=133:133"
	mass 350
}
actor RockBusterUpgrade_RespawnShadow : 8BDMItemRespawnShadow
{
	mass 350
	States
	{
		Spawn:
			RNCW I 0
			goto Super::Spawn
	}
}

actor RockBuster_MugshotColor : MugshotColor {args 102, 133}

Actor RockBuster : MegaBuster
{
	Tag "$TAG_ROCKBUSTER"
	Obituary "$OB_ROCKBUSTER"
	Inventory.Pickupmessage "$PU_ROCKBUSTER"
	Dropitem ""
	States
	{
		SpawnLoop:
			RNCW I 1
			stop

		Ready:
			NCH1 A 0 A_JumpIfInventory("BusterGiven",1,"TakeBusters")
			goto Ready2
		TakeBusters:
			NCH1 A 0 A_GiveInventory("TakeBuster", 1)
			goto Ready2
		Ready2:
			NCH1 A 0 ACS_NamedExecuteWithResult("core_weaponcolor",CLR_ROCKBUSTER)
			NCH1 A 1 A_WeaponReady
			goto Ready2+1

		Deselect:
			NCH1 A 0
			goto DeselectSwap
		Select:
			NCH1 A 0
			goto SelectSwap

		Fire:
			NCH1 A 0 A_JumpIfNoAmmo("NoAmmo")
			NCH1 A 0 A_PlaySoundEx("weapon/mbuster","Weapon")
			NCH1 A 0 A_FireCustomMissile("RockMegaShot",0,0,8,0)
			NCH1 BC 3
			NCH1 A 2
			NCH1 A 0 A_Refire
			goto Ready2+1

		NoAmmo:
			NCH1 A 1
			goto Ready2+1
	}
}

actor RockMegaShot : MegaShot
{
	damage (CallACS("rnc_cbm_damage",12))
	Obituary "$OB_ROCKBUSTER"
	Speed 27
}