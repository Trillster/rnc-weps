actor RumblingBangWep_NormalBar : NormalBar {args 224, 40}

actor RumblingBangWep : BaseMM8BDMWep
{
    //$Category RNC-Weapons
    //$Title Rumbling Bang
    //$Sprite RNCWE0
    Weapon.AmmoUse 4
    Weapon.AmmoGive 28
    Weapon.SlotNumber 5
    Tag "$TAG_RUMBLINGBANG"
    Obituary "$OB_RUMBLINGBANG"
    Inventory.Pickupmessage "$PU_RUMBLINGBANG"
    weapon.ammotype "RumblingBangAmmo"
    inventory.icon "RNCWP5"
    States
    {
        SpawnLoop:
            RNCW E -1
            loop

        Ready:
            NCH2 H 0 ACS_NamedExecuteWithResult("core_weaponcolor",CLR_RUMBLINGBANG)
            NCH2 H 1 A_WeaponReady
            goto Ready+1

        Deselect:
            NCH2 H 0
            goto DeselectSwap
        Select:
            NCH2 H 0
            goto SelectSwap

        Fire:
            NCH2 H 0 A_JumpIfNoAmmo("NoAmmo")
            NCH2 H 0 A_PlaySoundEx("weapon/mbuster","Weapon")
            NCH2 H 0 A_SpawnItemEx("RumblingBang",24,8,32,5,0,12)
            NCH2 H 0 A_TakeInventory("RumblingBangAmmo",4,TIF_NOTAKEINFINITE)
            NCH2 IJ 3
            NCH2 H 29
            NCH2 H 0 A_Refire
            goto Ready+1
            
        NoAmmo:
            NCH2 H 1 ACS_NamedExecute("core_noammo",0)
            goto Ready+1
    }
}

actor RumblingBangAmmo : Ammo {inventory.amount 1 inventory.maxamount 28}

const int WEPC_RNCRB_FXRISE = 15;

actor RumblingBang : BasicProjectile
{
    +BOUNCEONWALLS
    +BOUNCEONCEILINGS
    -NOGRAVITY
    DamageType "RumblingBang"
    Obituary "$OB_RUMBLINGBANG"
    damage (CallACS("rnc_cbm_damage",1))
    Radius 6
    Height 6
    Gravity 2.0

    var int user_dist;
    var int user_timer;
    States
    {
        Spawn:
            RNCP N 1
            Loop
        Death:
            TNT1 A 0 A_SetUserVar("user_dist",30)
            TNT1 A 0 A_SetUserVar("user_timer",0)
            TNT1 A 0 A_Quake(4,9,0,286)
        Floor_L:
            TNT1 A 0 A_Explode(CallACS("rnc_cbm_damage",20), 180, 0, 0, 40)
            TNT1 A 0 A_PlaySoundEx("misc/metdie","Weapon")
            TNT1 A 0 A_SpawnItemEx("BangTrailFX",user_dist,0,floorz-z+1,0,0,WEPC_RNCRB_FXRISE,0+30,1)
            TNT1 A 0 A_SpawnItemEx("BangTrailFX",user_dist,0,floorz-z+1,0,0,WEPC_RNCRB_FXRISE,60+30,1)
            TNT1 A 0 A_SpawnItemEx("BangTrailFX",user_dist,0,floorz-z+1,0,0,WEPC_RNCRB_FXRISE,120+30,1)
            TNT1 A 0 A_SpawnItemEx("BangTrailFX",user_dist,0,floorz-z+1,0,0,WEPC_RNCRB_FXRISE,180+30,1)
            TNT1 A 0 A_SpawnItemEx("BangTrailFX",user_dist,0,floorz-z+1,0,0,WEPC_RNCRB_FXRISE,240+30,1)
            TNT1 A 0 A_SpawnItemEx("BangTrailFX",user_dist,0,floorz-z+1,0,0,WEPC_RNCRB_FXRISE,300+30,1)
            TNT1 A 6
            TNT1 A 0 A_SetUserVar("user_dist", user_dist + 40) 
            TNT1 A 0 A_JumpIf(user_dist >= 30 + 40 * 4, "End")
            loop
        End:
            TNT1 A 0
            stop
    }
}

actor BangTrailFX : BasicGraphicEffect
{
    Reactiontime 6
    States
    {
        SpawnFrame:
            MMFX BC 2 A_Countdown
        Spawn_L:
            MMFX D 1 A_Countdown
            loop
        Death:
            MMFX EEEEE 1 A_SetScale(scalex-0.5,scaley-0.5)
            stop
    }
}