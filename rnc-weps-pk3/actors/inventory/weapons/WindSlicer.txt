actor WindSlicerWep_NormalBar : NormalBar {args 213, 95}

actor WindSlicerWep : BaseMM8BDMWep
{
    //$Category RNC-Weapons
    //$Title Wind Slicer
    //$Sprite RNCWG0
    +WEAPON.NOAUTOAIM
    Weapon.AmmoUse 2
    Weapon.AmmoGive 28
    Weapon.SlotNumber 4
    Tag "$TAG_WINDSLICER"
    Obituary "$OB_WINDSLICER"
    Inventory.Pickupmessage "$PU_WINDSLICER"
    weapon.ammotype "WindSlicerAmmo"
    inventory.icon "RNCWP7"
    States
    {
        SpawnLoop:
            RNCW G -1
            loop

        Ready:
            NCH3 C 0 ACS_NamedExecuteWithResult("core_weaponcolor",CLR_WINDSLICER)
            NCH3 C 1 A_WeaponReady
            goto Ready+1

        Deselect:
            NCH3 C 0
            goto DeselectSwap
        Select:
            NCH3 C 0
            goto SelectSwap

        Fire:
            NCH3 C 0 A_JumpIfNoAmmo("NoAmmo")
            NCH3 C 0 A_PlaySoundEx("weapon/airshooter","Weapon")
            NCH3 C 0 A_FireCustomMissile("WindSlicer",0,1,8,0)
            NCH3 CCCCC 0 A_FireCustomMissile("WindSlicer",random(8,-8),0,8,0,0,random(0,8))
            NCH3 DE 3
            NCH3 C 26
            NCH3 C 0 A_Refire
            goto Ready+1
            
        NoAmmo:
            NCH3 C 1 ACS_NamedExecute("core_noammo",0)
            goto Ready+1
    }
}

actor WindSlicerAmmo : Ammo {inventory.amount 1 inventory.maxamount 28}

actor WindSlicer : BasicProjectile
{
    +NOEXPLODEFLOOR
    damagetype "WindSlicer"
    Obituary "$OB_WINDSLICER"
    damage (CallACS("rnc_cbm_damage",12))
    Radius 6
    Height 7
    speed 34
    States
    {
        Spawn:
            RNCP RST 2
        SpawnLoop:
            RNCP RRSSTT 1 ThrustThingZ(0,2,0,1)
            goto SpawnLoop
        Death:
            RNCP UVW 2
            stop
    }
}