actor SmallHealthNC : SmallHealth
{
	//$Category RNC-Pickups
	States
	{
		SpawnLoop:
			NCI1 A 0
			NCI1 AB 6
			loop
	}
}

actor SmallHealthNC_Respawn : SmallHealth_Respawn {}
actor SmallHealthNC_RespawnShadow : SmallHealth_RespawnShadow
{
	States
	{
		Spawn:
			NCI1 A 0
			goto 8BDMItemRespawnShadow::Spawn
	}
}

actor BigHealthNC : BigHealth
{
	//$Category RNC-Pickups
	accuracy 14
	States
	{
		SpawnLoop:
			NCI1 C 0
			NCI1 CD 6
			loop
	}
}

actor BigHealthNC_Respawn : BigHealth_Respawn {}
actor BigHealthNC_RespawnShadow : BigHealth_RespawnShadow
{
	States
	{
		Spawn:
			NCI1 C 0
			goto 8BDMItemRespawnShadow::Spawn
	}
}

actor SmallEnergyNC : WeaponEnergy
{
	//$Category RNC-Pickups
	States
	{
		SpawnLoop:
			NCI1 E 0
			NCI1 E 0 A_JumpIf(tid!=0, 2)
			NCI1 E 0 Thing_ChangeTID(0,999)
			NCI1 EF 6
			goto SpawnLoop+3
	}
}

actor SmallEnergyNC_Respawn : WeaponEnergy_Respawn {}
actor SmallEnergyNC_RespawnShadow : WeaponEnergy_RespawnShadow
{
	States
	{
		Spawn:
			NCI1 F 0
			goto 8BDMItemRespawnShadow::Spawn
	}
}

actor BigEnergyNC : BigWeaponEnergy
{
	//$Category RNC-Pickups
	States
	{
		SpawnLoop:
			NCI1 G 0
			NCI1 G 0 A_JumpIf(tid!=0, 2)
			NCI1 G 0 Thing_ChangeTID(0,999)
			NCI1 GH 6
			goto SpawnLoop+3
	}
}

actor BigEnergyNC_Respawn : BigWeaponEnergy_Respawn {}
actor BigEnergyNC_RespawnShadow : BigWeaponEnergy_RespawnShadow
{
	States
	{
		Spawn:
			NCI1 H 0
			goto 8BDMItemRespawnShadow::Spawn
	}
}