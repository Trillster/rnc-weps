actor WTankNC : WTank
{
	//$Category RNC-Items
	inventory.icon "RNCIT1"
	States
	{
		SpawnLoop:
			NCI1 I 0
			NCI1 I 0 A_JumpIf(tid!=0, 2)
			NCI1 I 0 Thing_ChangeTID(0,999)
			NCI1 IK 6
			goto SpawnLoop+3
		Success:
			NCI1 I 0 A_PlaySound("item/energyup")
			NCI1 I 0 A_PlaySoundEx("item/refill","Voice")
			NCI1 I 0 A_SpawnItemEx("WTankUse", 0, 0, 0, momx, momy, 0, 0, SXF_TRANSFERTRANSLATION|SXF_ABSOLUTEMOMENTUM)
			NCI1 I 0 A_GiveInventory("WTankAmmo",1)
			NCI1 I 0 A_TakeInventory("WTankNC",1)
			fail
		PickupUseSuccess:
			NCI1 I 0 A_GiveInventory("WTankAmmo",1)
			NCI1 I 0 A_SpawnItemEx("TakeWTankNC")
			stop
	}
}

actor TakeWTankNC : BasicWatcher
{
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 1 A_TakeFromTarget("WTankNC",1)
		Death:
			TNT1 A 0
			stop
	}
}

actor WTankNC_Respawn : WTank_Respawn {}
actor WTankNC_RespawnShadow : WTank_RespawnShadow
{
	States
	{
		Spawn:
			NCI1 I 0
			goto 8BDMItemRespawnShadow::Spawn
	}
}

actor ETankNC : ETank
{
	//$Category RNC-Items
	inventory.icon "RNCIT2"
	States
	{
		SpawnLoop:
			NCI1 J 0 
			EBAL J 0 A_JumpIf(tid!=0, 2)
			EBAL J 0 Thing_ChangeTID(0,999)
			NCI1 JK 6
			goto SpawnLoop+3
		Success:
			NCI1 J 0 A_PlaySound("item/energyup")
			NCI1 J 0 A_PlaySoundEx("item/refill","Voice")
			NCI1 J 0 A_SpawnItemEx("ETankUse",  0, 0, 0, momx, momy, 20, 0, SXF_TRANSFERTRANSLATION|SXF_ABSOLUTEMOMENTUM)
			NCI1 J 0 A_SpawnItemEx("ETankUse2", 0, 0, 0, momx, momy,  0, 0, SXF_TRANSFERTRANSLATION|SXF_ABSOLUTEMOMENTUM)
			NCI1 J 0 A_GiveInventory("ETankHeal",1)
			NCI1 J 0 A_TakeInventory("ETankNC",1)
			fail
	}
}

actor ETankNC_Respawn : ETank_Respawn {}
actor ETankNC_RespawnShadow : ETank_RespawnShadow
{
	States
	{
		Spawn:
			NCI1 J 0
			goto 8BDMItemRespawnShadow::Spawn
	}
}