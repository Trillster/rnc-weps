RNC weapons updated to MM8BDM V6B by Trillster!
Load with the RNC map pack for cool replacements and effects!

Credits to the original expansion team and those who have continued this pack.

=== Modder Notes ===
Translation IDs 3320-3336 used.
ClassBase Slot 31 used.

=== v4c -> v4d ===
- CBM CopyNerf updated to be in parity with future CBM.
- Moved Delay Flame to rapid slot for (T)LMS.
-- This now matches the slot defined on the weapon.
- Added RNC item variants to item table.
-- Marked as map invalid to not appear in randomizer.

=== v4b -> v4c ===
-Added compatibility with Class Based Modification (CBM)
-Reformatted DECORATE and ACS files
-Optimized RNC map replacement script.
-Circuit Board pickup now removes extra flags

=== v4a -> v4b ===
-Renamed some files to make them more specific to RNC
-Large HP pickup has adjusted respawn cooldown properties
-RNC Item-1 matches V6B Item-1 now
--Removed from assist item and training pool to reduce dupes.
